import Swiper from 'swiper/bundle';

export default class Index {
  /**
   * Creates an instance of Index.
   */
  constructor() {
    new Header();
    new HeaderFix();
    new Menu();
    new Gallery();
    new Maps();
    new Slide();
    new Video();
  }
}

class Header {
  constructor() {
    this.header = document.querySelector('#header')
    this.logo = document.querySelector('.header-logo')
    this.mv = document.querySelector('.mv')
    window.addEventListener('scroll', (e) => {
      this.showHeader(window.pageYOffset);
    })
  }
  showHeader(y) {
    let header = document.querySelector('#header')
    let logo = document.querySelector('.header-logo')
    let mv = document.querySelector('.mv')
    if (window.innerWidth > 768) {
      if (y >= (mv.clientHeight)) {
        header.classList.add('show')
        logo.classList.add('hide')
      } else {
        header.classList.remove('show')
        logo.classList.remove('hide')
      }
    }
  }
}

class HeaderFix {
  constructor() {
    this.header = document.getElementById('header');
    window.addEventListener('scroll', () => {
      this.handleX();
      this.handleY();
    });
  }
  handleX() {
    let scrollX = document.documentElement.scrollLeft || document.body.scrollLeft;
    this.header.style.left = -scrollX + 'px';
  }
  handleY() {
    let scrollY = window.scrollY || window.pageYOffset;
    let header = document.querySelector('#header')
    if (window.innerWidth < 769) {
      if (scrollY > header.clientHeight) {
        header.classList.add('is-fixed')
      } else {
        header.classList.remove('is-fixed')
      }
    }
  }
}

class Gallery {
  constructor() {
    this.Slider();
  }
  Slider() {
    let swiper = new Swiper('.introduce__slide', {
      loop: true,
      spaceBetween: -5,
      autoplay: {
        delay: 0,
        disableOnInteraction: false
      },
      allowTouchMove: false,
      speed: 10000,
      breakpoints: {
        320: {
          slidesPerView: 1,
        },
        769: {
          slidesPerView: 3,
        },
      }
    });
  }
}

class Menu {
  constructor() {
    this.trigger = document.querySelector('#trigger');
    this.wrap = document.querySelector('.header-menu');
    this.trigger.addEventListener('click', this.handle.bind(this));
  }
  handle() {
    if (!this.trigger.classList.contains('is-active')) {
      this.open();
    } else {
      this.close();
    }
  }
  open() {
    this.trigger.classList.add('is-active');
    this.wrap.classList.add('active');
  }
  close() {
    this.trigger.classList.remove('is-active');
    this.wrap.classList.remove('active');
  }
}

class Maps {
  constructor() {
    const example = {
      lat: 36.55789067614142,
      lng: 139.92758886946885
    };
    const mapStyle = [{
        elementType: 'geometry',
        stylers: [{
          color: '#333333'
        }]
      },
      {
        elementType: 'labels.icon',
        stylers: [{
          visibility: 'off'
        }]
      },
      {
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#616161'
        }]
      },
      {
        elementType: 'labels.text.stroke',
        stylers: [{
          color: '#333333'
        }]
      },
      {
        featureType: 'administrative.land_parcel',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#bdbdbd'
        }]
      },
      {
        featureType: 'poi',
        elementType: 'geometry',
        stylers: [{
          color: '#eeeeee'
        }]
      },
      {
        featureType: 'poi',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#757575'
        }]
      },
      {
        featureType: 'poi.park',
        elementType: 'geometry',
        stylers: [{
          color: '#e5e5e5'
        }]
      },
      {
        featureType: 'poi.park',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#9e9e9e'
        }]
      },
      {
        featureType: 'road',
        elementType: 'geometry',
        stylers: [{
          color: '#5b5b5b'
        }]
      },
      {
        featureType: 'road.arterial',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#757575'
        }]
      },
      {
        featureType: 'road.highway',
        elementType: 'geometry',
        stylers: [{
          color: '#ff0000'
        }]
      },
      {
        featureType: 'road.highway',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#616161'
        }]
      },
      {
        featureType: 'road.local',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#9e9e9e'
        }]
      },
      {
        featureType: 'transit.line',
        elementType: 'geometry',
        stylers: [{
          color: '#e5e5e5'
        }]
      },
      {
        featureType: 'transit.station',
        elementType: 'geometry',
        stylers: [{
          color: '#eeeeee'
        }]
      },
      {
        featureType: 'water',
        elementType: 'geometry',
        stylers: [{
          color: '#c9c9c9'
        }]
      },
      {
        featureType: 'water',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#9e9e9e'
        }]
      }
    ];
    const map = new google.maps.Map(document.getElementById('maps'), {
      zoom: 15,
      center: example,
      mapTypeControl: true,
      styles: mapStyle
    });
    const image = './assets/images/map_logo.png';
    const marker = new google.maps.Marker({
      position: example,
      map,
      icon: image
    });
  }
}

class Slide {
  constructor() {
    this.Models();
  }
  Models() {
    let swiper = new Swiper('.models', {
      initialSlide: 0,
      autoHeight: true,
      centeredSlides: true,
      autoplay: false,
      slidesPerView: 1,
      spaceBetween: 0,
      breakpoints: {
        320: {
          spaceBetween: 50,
        },
        769: {
          spaceBetween: 0,
        },
      },
      pagination: {
        el: ".swiper-pagination",
        type: 'bullets',
        clickable: true,
        renderBullet: function (index, className) {
          let car = document.querySelectorAll('.models__item')
          let arr1 = []
          let arr2 = []
          car.forEach((eles, stt) => {
            let carCategory = eles.getAttribute('data-category')
            arr1.push(carCategory)
            let carName = eles.getAttribute('data-name')
            arr2.push(carName)
          })
          return `<h4 class="paginate ${className}"> <span class="paginate-cate">${arr1[index]}</span><span class="paginate-name">${arr2[index]}</span></h4>`;
        },
      },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
    });
  }
}

class Video {
  constructor() {
    this.video = document.querySelector('#video')
    this.pauseStream(this.video);
  }
  pauseStream(stream) {
    let wrapVideo = document.querySelector('.introduce__movie')
    let iconPlay = document.querySelector('.fav-play')
    stream.addEventListener("play", function (e) {
      console.log('play nha');
      wrapVideo.classList.add('playing')
    });
    stream.addEventListener("pause", function (e) {
      console.log('pause nha');
      wrapVideo.classList.remove('playing')
    });
  }

}